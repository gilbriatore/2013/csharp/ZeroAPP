﻿using System;
using System.Drawing;
using System.Windows.Forms;

class Tela : Form
{
    private Button btnOK;

    static void Main(string[] s)
    {
        Application.Run(new Tela());
    }

    private void InitializeComponent()
    {
        this.btnOK = new System.Windows.Forms.Button();
        this.SuspendLayout();
        // 
        // btnOK
        // 
        this.btnOK.Location = new System.Drawing.Point(84, 46);
        this.btnOK.Name = "btnOK";
        this.btnOK.Size = new System.Drawing.Size(75, 23);
        this.btnOK.TabIndex = 0;
        this.btnOK.Text = "OK";
        this.btnOK.UseVisualStyleBackColor = true;
        // 
        // Tela
        // 
        this.ClientSize = new System.Drawing.Size(585, 112);
        this.Controls.Add(this.btnOK);
        this.Name = "Tela";
        this.ResumeLayout(false);

    }
}